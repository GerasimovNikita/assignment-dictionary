global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int
global string_equals
global string_copy
global print_error
section .text
 
 
; Принимает код возврата и завершает текущий процесс
; rdi - exit code
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0 ; comparing byte to null-terminator
        je .end ; if equal to it go to end of function
        inc rax ; else increase counter and go loop
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rdi - string pointer
print_string:
    call string_length ; find string length
    mov rdx, rax ; put it in length argument
    mov rax, 1
    mov rsi, rdi ; put pointer to string begining
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
; rdi - symbol code
print_char:
    mov rax, 1
    push rdi ; push symbol to stack, cause we need pointer on symbol
    mov rsi, rsp ; put pointer to symbol
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi ; return sympol from stack to return from function without problems
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA ; call "print_char" function with 0xA argument
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; rdi - input number
print_uint:
    mov rax, rdi ; put input number
    mov r8, 10 ; put devider
    mov r9, rsp ; save initial value of rsp
    dec rsp ; put null-terminator to the end of string in stack
    mov [rsp], byte 0
    .division:
        xor rdx, rdx ; put 0 in mod of division
        div r8 ; divide rax (number) by 10
        add dl, '0' ; make the mod an ASCII symbol
        dec rsp ; put it in stack
        mov [rsp], dl
        test rax, rax ; check, if given number ended
        jnz .division
    mov rdi, rsp ; call "print_string" with pointer on number in stack
    call print_string
    mov rsp, r9 ; return initial rsp value
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
; rdi - input number
print_int:
    test rdi, rdi ; check if it positive -> call "print_uint"
    jns print_uint
    push rdi
    mov rdi, '-' ; save rdi, and call "print_char" to print minus
    call print_char
    pop rdi
    neg rdi ; return rdi and call "print_uint" with two's complement of abs of this number
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - 1 string pointer
; rsi - 2 string pointer
string_equals:
    mov al, byte[rdi]
    cmp al, byte[rsi] ; comparing bytes of strings
    jne .not_equal
    inc rdi
    inc rsi
    test al, al
    jnz string_equals
    mov rax, 1
    ret
    .not_equal:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    mov rdi, 0
    push 0
    mov rsi, rsp ; read char in stack
    mov rdx, 1
    syscall
    pop rax ; take it from stack
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi - buf begining
; rsi - buf size
read_word:
    push r12
    push r13
    push rbx
    xor rbx, rbx ; symbol counter
    mov r12, rdi
    mov r13, rsi
    .delete_space_chars: ; check spaces
        call read_char
        cmp rax, 0x20
        je .delete_space_chars
        cmp rax, 0x9
        je .delete_space_chars
        cmp rax, 0xA
        je .delete_space_chars
        cmp rax, 0
        je .end_of_word
    .read_word_loop:
        cmp rbx, r13 ; check if word length bigger than buf size
        je .end_of_buf
        mov [r12 + rbx], al ; put char in buf
        inc rbx ; increase counter
        call read_char ; check end of word
        cmp rax, 0xA
        je .end_of_word
        cmp rax, 0
        je .end_of_word
        jmp .read_word_loop
    .end_of_word:
        mov [r12 + rbx], byte 0 ; add null-terminator
        mov rax, r12 ; put pointer to buf
        mov rdx, rbx ; put word length
        pop rbx
        pop r13
        pop r12
        ret
    .end_of_buf:
        pop rbx
        pop r13
        pop r12
        xor rax, rax
        ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; rdi - string pointer
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    mov rsi, 10
    .number_char_loop:
        mov cl, byte[rdi + rdx] ; read one char
        cmp cl, '0' ; check if it is a digit
        jl .end_of_uint
        cmp cl, '9'
        jg .end_of_uint
        sub cl, '0' ; make it number
        imul rax, 10 ; move all stored digits on higher places
        add rax, rcx ; add new read digit
        inc rdx ; increase lenght counter
        jmp .number_char_loop
    .end_of_uint:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; rdi - string pointer
parse_int:
    xor rax, rax
    mov al, byte[rdi] ; check first char 
    cmp al, '-'
    jne .positve_int ; if it is not minus parse like unsigned
    inc rdi ; increase pointer (for not prsing sign)
    call parse_uint
    inc rdx ; increase length (for sign)
    neg rax; make value negative (cause it was negative number)
    ret
    .positve_int:
        call parse_uint
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi - string pointer
; rsi - buf begining
; rdx - buf size
string_copy:
    xor rax, rax
    xor rcx, rcx
    xor r8, r8
    .copy_loop:
        cmp rax, rdx ; check if string length bigger than buf size
        je .little_buf
        mov r8b, [rdi + rax] ; get next char
        mov [rsi + rax], r8b ; put in buf next char
        cmp r8, 0 ; check null-terminator
        je .end_of_copy
        inc rax ; increase length counter
        jmp .copy_loop
    .little_buf:
        xor rax, rax
        ret
    .end_of_copy:
        ret
; rdi - error msg
print_error: ; the same as print_string but another descriptor (2 - err)
	call string_length
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, 2
	syscall
	ret
