global find_word

extern string_equals

section .text

; rdi - string pointer
; rsi - dictionary begining pointer
find_word:
	test rsi, rsi ; check if reached end of dict (or if there was no words)
	jz .not_found
	push rdi ; save pointer on word before func
	push rsi ; save current dict pointer before func
	add rsi, 8 ; add size of dq to get key pointer
	call string_equals
	test rax, rax ; check if found key
	pop rsi 
	pop rdi
	jne .found
	mov rsi, [rsi] ; move dict pointer
	jmp find_word
	.found:
		mov rax, rsi
		ret
	.not_found:
		xor rax, rax
		ret
