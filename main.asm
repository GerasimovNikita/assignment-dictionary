%include "colon.inc"
%include "words.inc"
%include "lib.inc"

global _start

%define buf_size 256

section .rodata
input_msg: db "Write key: ", 0
success_msg: db "Found key. It's value: ", 0
failure_msg: db "Not found key. Sorry.",10, 0
buf_overflow_msg: db "Key is longer than 255 symbols.",10, 0
empty_key_msg: db 10, "Key is empty.",10, 0

section .text
_start:
	mov rdi, input_msg ; asking for key
	call print_string
	sub rsp, buf_size ; create buf
	mov rdi, rsp ; put buf pointer
	mov rsi, buf_size ; put buf size
	call read_word
	test rax, rax ; check if could read key in buf
	jz .buf_error
	test rdx, rdx ; check if written eof
	jz .empty_key_error
	mov rdi, rax ; put pointer on key in buf
	mov rsi, word_label ; put pointer on dict begining
	call find_word 
	test rax, rax ; check success of finding key
	jz .not_found_key
	add rsp, buf_size ; delete buf(return initial stack pointer)
	add rax, 8 ; add size of dq to pointer of key
	push rax ; save it before calling funcs
	mov rdi, success_msg
	call print_string
	pop rdi ; put pointer on key
	push rdi ; save it again before func
	call string_length
	pop rdi
	inc rax ; increase length (for null-terminator)
	add rdi, rax ; add length of key (pointer on data after key)
	call print_string
	call print_newline
	call exit
	.buf_error:
		add rsp, buf_size
		mov rdi, buf_overflow_msg
		call print_error
		call exit
	.empty_key_error:
		add rsp, buf_size
		mov rdi, empty_key_msg
		call print_error
		call exit
	.not_found_key:
		add rsp, buf_size
		mov rdi, failure_msg
		call print_error
		call exit
	
