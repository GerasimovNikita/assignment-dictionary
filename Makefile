ASM=nasm
ASMFLAGS=-f elf64
LD=ld
main: main.o lib.o dict.o
	$(LD) -o $@ $^
lib.o: lib.asm

dict.o: dict.asm

main.o: main.asm colon.inc words.inc lib.inc

clean:
	rm -rf *.o main
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
